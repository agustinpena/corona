#! /Library/Frameworks/Python.framework/Versions/3.8/bin/python3
# Retrives data about conronavirus per country
# The used API is below
# https://www.programmableweb.com/api/about-corona-covid-19-rest-api-v1
# REQUIRED MODULES: requests, tabulate, colored, datetime

# TO DO:
# +. program does not work with country_code = 'bl', 'vi', etc.
#    Introduce an exception to go around this

# import required  modules
from requests import get
from tabulate import tabulate
from colored import fg, attr
import datetime

# Defines a function to format time and date


def format_date(cad):
    dt = cad.split('T')
    return dt[0] + ', ' + dt[1][:5] + 'Z'

# Defines functions to format strings for color


def c_yellow(cad):
    return fg('light_yellow') + cad + attr('reset')


def c_red(cad):
    return fg('red_1') + cad + attr('reset')


def c_blue(cad):
    return fg('aquamarine_1a') + cad + attr('reset')


def main():
    # Setting headers and colors for tabulation
    hs = ['Country', 'Infected', 'Recovered',
          'Total Dths', 'New Dths']
    hdrs = [c_yellow(x) for x in hs]

    # print program title
    print('')
    print(c_yellow('*** CORONAVIRUS DATA PER COUNTRY ***'))
    # starts main loop
    while True:
        # prompts for country code or q option
        country_code = input('Enter ISO 2-digit country code [(q)uit]: ')

        if country_code == 'q' or country_code == '':
            # quit the program if option is 'q'
            print(c_yellow('GOOD BYE!'))
            break
        else:
            # connects to the api and requests data
            endpoint = 'http://corona-api.com/countries/' + country_code
            response = get(endpoint)
            if response.status_code != 200:
                # if response is bad then give warning,
                # break loop and go back to main prompt
                print('')
                print(c_yellow('COULD NOT ESTABLISH A CONNECTION'))
                print(c_yellow('PLEASE CHECK THE COUNTRY CODE AND TRY AGAIN'))
                print('')
            else:
                # if response is fine, then:
                # retrieve info from the json file
                info = response.json()
                # determine the contry name
                country = info['data']['name']
                # determine the dictionary with the relevant information
                updated = info['data']['timeline'][0]
                # format to tabulate with 'tabulate'
                list_ct = [(country,
                            updated['confirmed'],
                            updated['recovered'],
                            c_red(str(updated['deaths'])),
                            c_red(str(updated['new_deaths'])))]

                print('')
                # format last update time and date
                head_1 = c_yellow('UPDATED:')
                formatted_date = c_blue(format_date(updated['updated_at']))
                # get local date and time
                head_2 = c_yellow(' LOCAL TIME:')
                x = c_blue(str(datetime.datetime.now()))
                print(head_1 + formatted_date + head_2 + x)
                # tabulate and print the info
                print(tabulate(list_ct, headers=hdrs, tablefmt='grid'))
                print('')


if __name__ == '__main__':
    main()
